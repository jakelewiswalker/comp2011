"""empty message

Revision ID: 40af91e081ae
Revises: 9e91b7ac2f37
Create Date: 2019-10-20 19:58:18.527586

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '40af91e081ae'
down_revision = '9e91b7ac2f37'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('tasks', sa.Column('description', sa.String(length=500), nullable=True))
    op.drop_index('ix_tasks_task', table_name='tasks')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_index('ix_tasks_task', 'tasks', ['task'], unique=1)
    op.drop_column('tasks', 'description')
    # ### end Alembic commands ###
