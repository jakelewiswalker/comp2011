from app import db

class Tasks(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    task = db.Column(db.String(500))
    description = db.Column(db.String(500))
    completed = db.Column(db.Integer)
