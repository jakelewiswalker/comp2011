from flask_wtf import Form
from wtforms import IntegerField, StringField
from wtforms.validators import DataRequired

class newTaskForm(Form):
    task1 = StringField('task1', validators=[DataRequired()])
    task2 = StringField('task2', validators=[DataRequired()])
