from app import app
from flask import Flask, render_template, flash
from .forms import newTaskForm
from app import db, models
from flask import request, redirect
from sqlalchemy import update


@app.route('/index', methods=['GET', 'POST'])
def index():
    listdb = models.Tasks.query.all()
    id = request.args.get('id')
    if id:
        task = models.Tasks.query.get(id)
        task.completed = 1
        db.session.commit()
        return redirect("/index")
    return render_template('index.html.twig',
                            id = id,
                            listdb = listdb,
                            active = 'index',
                            title='To Do - View All Tasks')

@app.route('/completed')
def completed():
    listdb = models.Tasks.query.all()
    id = request.args.get('id')
    if id:
        task = models.Tasks.query.get(id)
        db.session.delete(task)
        db.session.commit()
        return redirect("/completed")
    return render_template('completed.html.twig',
                            active = 'completed',
                            listdb = listdb,
                            title='To Do - View CompletedTasks')


@app.route('/add', methods=['GET', 'POST'])
def add():
    form = newTaskForm()
    if form.validate_on_submit():
        flash('<br><div class="alert alert-success" role="alert"> Successfully added task: %s to your to do list!</div>'%(form.task1.data))
        value = models.Tasks(task=form.task1.data, description=form.task2.data ,completed=0)
        db.session.add(value)
        db.session.commit()
    return render_template('add.html.twig',
                           active = 'add',
                           title='To Do - Add Task',
                           form=form)
